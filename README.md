# cnc

En este sitio se encuentra un proyecto más sobre diseño de
una máquina de Control Numérico Computacional (CNC).

Se basa en aportaciones que la comunidad hace bajo la filosofía de hardware y software libre. Por lo que no es un proyecto original desarrollado desde cero (si es que eso es posible en cualquier proyecto) sino algo que considero el hola mundo en los terrenos CNC.

Hay que mencionar como fuente primigenia de inspiración el proyecto [RepRap](https://reprap.org/wiki/RepRap/es), sobra recordar que "RepRap es la primera máquina auto-replicante de uso general de la humanidad".

Como ya se mencionó este "Hola mundo" es la base para juntar las piezas del rompecabezas conformado por la mecánica, el acceso a hardware libre para control de motores a pasos, la interpretación de los códigos g, el tratamiento apropiado para convertir los archivos SVG en dibujos grabados, los gerber en tarjetas de circuito impreso, etc. para llegar en algún momento a la impresión 3D.

Para contribuír a lo que Juan González-Gómez ObiJuan llama "patrimonio libre de la humanidad" se incluyen todos los editables de los archivos generados y se documenta con el mayor detalle posible el proceso de ensamble.

Este modelo es un clon del propuesto por el [Profe García](http://elprofegarcia.com/) quien muestra un panorama completo del proceso con herramientas de software libre. Dada la dificultad por conseguir algunas piezas en la CDMX se hacen ligeras modificaciones que permiten concluir el ensamble.

La intención es elaborar este proyecto con herramientas computacionales que
sustentan la filosofía de Software Libre.
Puede leerse más en el sitio de la Free software Foundation [(FSF)](fsf.org)

En ese sentido esta guía es también libre en los términos de la licencia
Creative Commons, no así el modelo que describe que pertenece al Profe García.
